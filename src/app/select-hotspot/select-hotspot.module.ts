import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { SelectHotspotRoutingModule } from "./select-hotspot-routing.module";
import { SelectHotspotComponent } from "./select-hotspot.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SelectHotspotRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SelectHotspotComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SelectHotspotModule { }
