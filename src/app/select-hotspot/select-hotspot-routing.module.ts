import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SelectHotspotComponent } from "./select-hotspot.component";

const routes: Routes = [
    { path: "", component: SelectHotspotComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SelectHotspotRoutingModule { }
