import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ListPicker } from "ui/list-picker";
import { MonitorResetService } from "../shared/monitor-reset/monitor-reset.service";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "SelectHotspot",
    moduleId: module.id,
    templateUrl: "./select-hotspot.component.html",
})
export class SelectHotspotComponent implements OnInit {
    ssids: Array<string> = [];
    chosenSsid: string;
    instructions: string = "Please select the WiFi network the monitor will use. Do NOT select a 5G or public network as the monitor can not broadcast on these networks."
    networkPassword: string;
    constructor(private monitorResetService: MonitorResetService, private router: Router, private routerExtensions: RouterExtensions) {

    }

    ngOnInit(): void {
        this.monitorResetService.currentSsids.subscribe((data) => {
            console.log("SSIDs update", data);
            this.ssids = data;

        });
    }
    selectedIndexChanged(args) {
        const picker = <ListPicker>args.object;
        this.chosenSsid = this.ssids[picker.selectedIndex];
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
    sendCredentials() {
        console.log("these are cred", this.chosenSsid, this.networkPassword)
        this.monitorResetService.sendMonitorResponse({
            ssid: this.chosenSsid,
            password: this.networkPassword
        }).then((res)=> {
            console.log("this was successful in resting monitor", res);
            this.router.navigate(["/prime750"]);
        }).catch((error) => {console.log("There was an error setting up monitor", error);
            this.router.navigate(["/prime750"]);
        })

    }
}
