import {  AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import * as app from "application";
import * as email from "nativescript-email";
import * as TNSPhone from "nativescript-phone";
import { RadialNeedle } from "nativescript-ui-gauge";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { SwipeGestureEventData } from "tns-core-modules/ui/gestures";
const utilityModule = require("utils/utils");
import { UserService } from "~/app/shared/user/user.service";
import { SummaryService } from "../shared/summary/summary.service";

@Component({
    selector: "Summary",
    moduleId: module.id,
    templateUrl: "./summary.component.html"
})
export class SummaryComponent implements OnInit, AfterViewInit  {
    currentTank: number;
    consumerTanks: any;
    tankName: string = "Tanks Loading";
    lastDeliveryDate: string =  "Last Delivery: N/A" ;
    nextDeliveryDate: string = "Next Appx. Delivery: N/A";
    logoUrl: string = "";
    loading: string = "loading";
    processing: boolean = true;
    composeOptions: email.ComposeOptions;
    dealerPhone: string;
    dealerUrl: string;
    @ViewChild("needle") needleElement: ElementRef;
    private _needle: RadialNeedle;

    constructor(private summaryService: SummaryService, private userService: UserService, private router: Router) {

    }

    ngAfterViewInit() {
        this._needle = this.needleElement.nativeElement as RadialNeedle;

    }
    nextTank() {
        if (this.currentTank >= this.consumerTanks.length - 1) {
                this.currentTank = 0;
        } else {
                this.currentTank++;
        }
        const selectedTank = this.consumerTanks[this.currentTank];
        this.changeTankDetails(selectedTank);
    }
    previousTank() {
        if (this.currentTank <= 0) {
            this.currentTank = this.consumerTanks.length - 1;
        } else {
            this.currentTank--;
        }
        const selectedTank = this.consumerTanks[this.currentTank];
        this.changeTankDetails(selectedTank);

    }
    changeTankDetails(selectedTank) {
        this.summaryService.changeTank(this.currentTank);
        this._needle.value = selectedTank.percent;
        this.tankName = selectedTank.name;
        this.lastDeliveryDate = `Last Delivery: ${selectedTank.lastDeliveryDate}`;
        this.nextDeliveryDate = `Next Appx. Delivery: ${selectedTank.nextDeliveryDate}`;
    }
    ngOnInit(): void {
        this.userService.currentLogoUrl.subscribe((url) => {
            console.log("Logo Url for gauge", url);
            this.logoUrl =  url;
        });
        this.summaryService.currentdealerInfo.subscribe((data) => {
            console.log("Dealer Information", data);
            this.composeOptions = {
                to: [data.email]
            };

            this.dealerPhone = data.phone;
            this.dealerUrl = data.url;
        });
        this.summaryService.currentTank.subscribe((tankNum) => {
            this.currentTank =  tankNum;

        });

        this.getDealerInfo();
        this.getConsumerInfo();
    }

    onSwipe(args: SwipeGestureEventData) {
        if (args.direction === 1) {
            this.previousTank();

        } else if (args.direction === 2) {
            this.nextTank();
        }
    }
    getConsumerInfo() {
        this.summaryService.loadConsumerData()
            .then((data) => {
                this.processing = false;
                this.loading = "finished";

                this.summaryService.currentConsumerInfo.subscribe((consumerInfo) => {
                    console.log("Current Consumer Info", consumerInfo);
                    this.consumerTanks = consumerInfo.tanks;

                    const selectedTank = this.consumerTanks[this.currentTank];
                    console.log("this is the initial load of tanks", selectedTank);
                    this.changeTankDetails(selectedTank);

                });
            }).catch((error) => {
                this.loading = "finished";
                console.log("Consumer Information", error);
            });

    }

    getDealerInfo() {
        this.summaryService.loadDealerData().then(
            (response) => {
            }).catch((error) => console.log(" Dealer Information", error));

    }

    goToProfile() {
        this.router.navigate(["/profile"]);
    }

    sendEmail() {
        email.available().then((available) => {

            if (available) {
                email.compose(this.composeOptions).then((result) => {
                    console.log("Email Sent", result);
                }).catch((error) => console.error(error));
            }
        }).catch((error) => console.log("email error", error));
    }

    callDealer() {

        TNSPhone.dial(this.dealerPhone, true);
    }

    launchWebSite() {
        utilityModule.openUrl(this.dealerUrl);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
