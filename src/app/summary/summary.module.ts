import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { FeaturedRoutingModule } from "./summary-routing.module";
import { SummaryComponent } from "./summary.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        FeaturedRoutingModule,
        NativeScriptUIGaugeModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SummaryComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SummaryModule { }
