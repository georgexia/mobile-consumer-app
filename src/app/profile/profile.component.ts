import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Router } from "@angular/router";
import * as email from "nativescript-email";
import * as TNSPhone from "nativescript-phone";
import { SummaryService } from "~/app/shared/summary/summary.service";
const utilityModule = require("utils/utils");

@Component({
    selector: "Profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html"
})
export class ProfileComponent implements OnInit {
    composeOptions: email.ComposeOptions;
    customerAccount: string;
    customerName: string;
    customerPhone: string;
    customerAddressStreet1: string;
    customerAddressStreet2: string;
    dealerPhone: string;
    dealerUrl: string;

    constructor(private summaryService: SummaryService, private router: Router) {

    }
    ngOnInit(): void {
        // Init your component properties here.
        this.summaryService.currentdealerInfo.subscribe((data) => {
            console.log("Dealer Information", data)
            // this.dealerEmail = data.email;
            this.composeOptions = {
                to: [data.email]
            }
            this.dealerPhone = data.phone;
            this.dealerUrl = data.url;
        });
        this.summaryService.currentConsumerInfo.subscribe((consumerInfo) => {
            console.log("Current Consumer Info for customer", consumerInfo.customer)
            const consumerInfoCustomer = consumerInfo.customer;
            this.customerAccount = consumerInfoCustomer.account;
            this.customerName = consumerInfoCustomer.name;
            this.customerPhone = consumerInfoCustomer.phone;
            this.customerAddressStreet1 = consumerInfoCustomer.address.line1 + consumerInfoCustomer.address.line2;
            this.customerAddressStreet2 = consumerInfoCustomer.address.city + " ," + consumerInfoCustomer.address.state + " " + consumerInfoCustomer.address.postalCode;
        })
    }

    goToSummary() {
        this.router.navigate(["/summary"]);
    }
    sendEmail() {
        email.available().then((available) => {

            if(available){
                email.compose(this.composeOptions).then((result) => {
                    console.log("Email Sent",result);
                }).catch(error => console.error(error));
            }
        }).catch((error) => console.log("email error", error));
    }

    callDealer() {

        TNSPhone.dial(this.dealerPhone, true);
    }

    launchWebSite() {
        utilityModule.openUrl(this.dealerUrl);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
