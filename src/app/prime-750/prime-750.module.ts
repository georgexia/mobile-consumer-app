import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { Prime750RoutingModule } from "./prime-750-routing.module";
import { Prime750Component } from "./prime-750.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        Prime750RoutingModule
    ],
    declarations: [
        Prime750Component
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class Prime750Module { }
