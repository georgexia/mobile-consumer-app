import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "Prime750",
    moduleId: module.id,
    templateUrl: "./prime-750.component.html"
})
export class Prime750Component implements OnInit {
instructions:string = "The monitor light will turn green for 15 seconds and then flash either or red. Be sure to count the number of flashes. Confirm that the GREMLIN® displayed five green flashes. This indicates that the monitor has been properly set up."
    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    continue(){
        this.router.navigate(["/resetSuccess"]);
    }
    reenter() {
        this.router.navigate(["/primeMonitor"]);

    }
    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
