import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { CommsService } from "./shared/comms";
import { MonitorResetService } from "./shared/monitor-reset/monitor-reset.service";
import { SummaryService } from "./shared/summary/summary.service";
import { UserService } from "./shared/user/user.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { MyHttpInterceptor } from "./shared/httpinterceptor";


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        HttpClientModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [CommsService, SummaryService, UserService, MonitorResetService, {
        provide: HTTP_INTERCEPTORS,
        useClass: MyHttpInterceptor,
        multi: true
    }]
})
export class AppModule { }
