import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { PrimeMonitorComponent } from "./prime-monitor.component";

const routes: Routes = [
    { path: "", component: PrimeMonitorComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PrimeMonitorRoutingModule { }
