import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Router } from "@angular/router";

@Component({
    selector: "PrimeMonitor",
    moduleId: module.id,
    templateUrl: "./prime-monitor.component.html"
})
export class PrimeMonitorComponent implements OnInit {
    instructions: string = "Press the button or touch the magnet to the notch on the side of the monitor until the light turns from";

    constructor(private router: Router) {
        // Use the component constructor to inject providers.

    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    continue() {
        this.router.navigate(["/monitorHotspot"]);
    }
}
