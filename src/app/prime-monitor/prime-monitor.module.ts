import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { PrimeMonitorRoutingModule } from "./prime-monitor-routing.module";
import { PrimeMonitorComponent } from "./prime-monitor.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        PrimeMonitorRoutingModule
    ],
    declarations: [
        PrimeMonitorComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class PrimeMonitorModule { }
