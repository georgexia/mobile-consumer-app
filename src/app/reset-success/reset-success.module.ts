import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ResetSuccessRoutingModule } from "./reset-success-routing.module";
import { ResetSuccessComponent } from "./reset-success.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ResetSuccessRoutingModule
    ],
    declarations: [
        ResetSuccessComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ResetSuccessModule { }
