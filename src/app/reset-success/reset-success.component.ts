import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "ResetSuccess",
    moduleId: module.id,
    templateUrl: "./reset-success.component.html"
})
export class ResetSuccessComponent implements OnInit {
    instructions: string = "Your GREMLIN� WiFi settings have been updated successfully!";
    constructor(private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    continue() {
        this.router.navigate(["/summary"]);
    }
    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
