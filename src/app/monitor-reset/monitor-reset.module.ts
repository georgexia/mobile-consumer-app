import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MonitorResetRoutingModule } from "./monitor-reset-routing.module";
import { MonitorResetComponent } from "./monitor-reset.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MonitorResetRoutingModule
    ],
    declarations: [
        MonitorResetComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MonitorResetModule { }
