import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { MonitorResetService } from "../shared/monitor-reset/monitor-reset.service";

@Component({
    selector: "MonitorReset",
    moduleId: module.id,
    templateUrl: "./monitor-reset.component.html"
})
export class MonitorResetComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
