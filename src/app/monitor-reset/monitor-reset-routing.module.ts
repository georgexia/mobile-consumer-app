import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MonitorResetComponent } from "./monitor-reset.component";

const routes: Routes = [
    { path: "", component: MonitorResetComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MonitorResetRoutingModule { }
