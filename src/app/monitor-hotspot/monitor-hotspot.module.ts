import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { MonitorHotspotRoutingModule } from "./monitor-hotspot-routing.module";
import { MonitorHotspotComponent } from "./monitor-hotspot.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        MonitorHotspotRoutingModule
    ],
    declarations: [
        MonitorHotspotComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class MonitorHotspotModule { }
