import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { MonitorResetService } from "../shared/monitor-reset/monitor-reset.service";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "MonitorHotspot",
    moduleId: module.id,
    templateUrl: "./monitor-hotspot.component.html"
})
export class MonitorHotspotComponent implements OnInit {
    processing: boolean = false;
    instructions: string = "Go to your phone/laptop network settings and connect to the WiFi network starting with PAYGO. Once you have connected press continue to configure the monitor.";
    constructor(private monitorResetService: MonitorResetService, private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
    continue() {
        this.processing = true;
        this.monitorResetService.getMonitorId().then((res) =>{
            this.processing = false;
            console.log("this is the response before we route over to select Hotspot", res)
            if (res.length > 0) {
                console.log("this went into navigation")
                this.router.navigate(["/selectHotspot"]);
            }else {
                let options = {
                    title: "GREMLIN Monitor",
                    message: "We could not connect to the monitor or no WiFi network found",
                    okButtonText: "OK"
                };
                alert(options)
            }
        }).catch((error) => {
            console.log("line 11 error monitorid", error)
            this.processing = false;
            let options = {
                title: "GREMLIN Monitor",
                message: "We could not connect to the monitor. Please make sure you are connected to the GREMLIN Hotspot",
                okButtonText: "OK"
            };

            alert(options)

        })

        // This can be used to make sure it is a wifi
        // const connectionType = connectivityModule.getConnectionType();

        // switch (connectionType) {
        //     case connectivityModule.connectionType.none:
        //         // Denotes no Internet connection.
        //         console.log("No connection");
        //         break;
        //     case connectivityModule.connectionType.wifi:
        //         // Denotes a WiFi connection.
        //         console.log("WiFi connection", connectivityModule);
        //         break;
        //     case connectivityModule.connectionType.mobile:
        //         // Denotes a mobile connection, i.e. cellular network or WAN.
        //         console.log("Mobile connection");
        //         break;
        //     default:
        //         break;
        // }

    }
}
