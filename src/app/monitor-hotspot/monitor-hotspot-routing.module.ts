import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { MonitorHotspotComponent } from "./monitor-hotspot.component";

const routes: Routes = [
    { path: "", component: MonitorHotspotComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class MonitorHotspotRoutingModule { }
