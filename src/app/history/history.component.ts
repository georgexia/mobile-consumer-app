import { Component, OnInit } from "@angular/core";
import { SummaryService } from "../shared/summary/summary.service";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

@Component({
    selector: "History",
    moduleId: module.id,
    templateUrl: "./history.component.html"

})
export class HistoryComponent implements OnInit {
    tankReadings: Array<object>;
    loaded: boolean = false;
    consumerTanks = [];
    constructor(private summaryService: SummaryService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        this.summaryService.currentConsumerInfo.subscribe((consumerInfo) => {
            console.log("Current Consumer Info in historry", consumerInfo)
            this.consumerTanks = consumerInfo.tanks

        })
        this.summaryService.currentTank.subscribe((tankNum) => {
            console.log("current Tank Number in histoy", tankNum)
            this.tankReadings =  this.consumerTanks[tankNum].levels
            this.loaded = true;
        })


    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
