import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";


const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", loadChildren: "~/app/auth/login.module#LoginModule" },
    { path: "history", loadChildren: "~/app/history/history.module#HistoryModule" },
    { path: "monitorReset", loadChildren: "~/app/monitor-reset/monitor-reset.module#MonitorResetModule" },
    { path: "monitorHotspot", loadChildren: "~/app/monitor-hotspot/monitor-hotspot.module#MonitorHotspotModule" },
    { path: "selectHotspot", loadChildren: "~/app/select-hotspot/select-hotspot.module#SelectHotspotModule" },
    { path: "primeMonitor", loadChildren: "~/app/prime-monitor/prime-monitor.module#PrimeMonitorModule" },
    { path: "prime750", loadChildren: "~/app/prime-750/prime-750.module#Prime750Module" },
    { path: "resetSuccess", loadChildren: "~/app/reset-success/reset-success.module#ResetSuccessModule" },
    { path: "summary", loadChildren: "~/app/summary/summary.module#SummaryModule" },
    { path: "profile", loadChildren: "~/app/profile/profile.module#ProfileModule" },

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
