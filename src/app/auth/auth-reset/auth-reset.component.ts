import { Component, OnInit } from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { UserService } from "../../shared/user/user.service";

@Component({
    selector: "AuthReset",
    moduleId: module.id,
    templateUrl: "./auth-reset.component.html",
    providers: [UserService]
})
export class AuthResetComponent implements OnInit {
    instructions: string =  "Provide your username below. We will send you an email containing instructions on how to reset your password."
    username: string = "";
    constructor(private userService: UserService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    reset() {
        this.userService.reset(this.username)
            .then((res) =>  {
                console.log("this is resetting")
                // this.router.navigate(["/summary"]);

            }).catch((error) => console.log("Reset Error", error));
    }
}
