import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Switch } from "tns-core-modules/ui/switch";
import { User } from "../shared/user/user";
import { UserService } from "../shared/user/user.service";

const appSettings = require("application-settings");

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit {
    user: User;
    processing: boolean = false;
    errorMsg: string = "";
    rememberMe: boolean;
    constructor(private router: Router, private userService: UserService) {
        this.user = new User();
    }

    ngOnInit(): void {
        this.rememberMe = appSettings.getBoolean("isTurnedOn", false);
console.log("this is intial remember me value", this.rememberMe)
        if (this.rememberMe) {
            this.user.username = appSettings.getString("username");
            this.user.password = appSettings.getString("password");
        }
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onRemember(args){

        let secondSwitch = <Switch>args.object;
        console.log("this is the remember value changed", secondSwitch.checked)
        if (secondSwitch.checked) {
           // secondSwitch.checked;
            this.rememberMe = secondSwitch.checked;
            appSettings.setBoolean("isTurnedOn", secondSwitch.checked);
            appSettings.setString("username", this.user.username);
            appSettings.setString("password", this.user.password);

        } else {
            appSettings.setBoolean("isTurnedOn", false);
            appSettings.clear();

        }

    }
    onUsernameChange(args) {
        if (this.rememberMe){
            appSettings.setString("username", args.value);
        }
    }
    onPasswordChange(args) {
        if (this.rememberMe){
            appSettings.setString("password", args.value);
        }
    }
    submit() {
        this.errorMsg = "";
        this.processing = true;
        this.userService.login(this.user)
            .then((res) =>  {
                this.processing = false;
                this.userService.changeLoggedIn(true);
                this.router.navigate(["/summary"]);

    }).catch((error) => {
        this.processing = false;
        this.errorMsg = error.error.msg;
        console.log("Login", error);

    });

    }
    forgotPassword() {
        this.router.navigate(["/login/reset"]);
    }
    register() {

        this.router.navigate(["/login/register"]);
    }

}
