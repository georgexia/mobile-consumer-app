import { Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Switch } from "ui/switch";
import { UserService } from "../../shared/user/user.service";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { Router } from "@angular/router";

const utilityModule = require("utils/utils");

@Component({
    selector: "Register",
    moduleId: module.id,
    templateUrl: "./register.component.html"
})
export class RegisterComponent implements OnInit {
    instructions: string = "Please register below to get started!"
    username: string = "";
    password: string = "";
    confirmpassword: string = "";
    dealeraccountnumber: string = "";
    phonenumber: string = "";
    tankmonitorserial: string = "";
    email: string = "";
    error: string = "";
    switchState: boolean = false;

    constructor(private userService: UserService, private barcodeScanner: BarcodeScanner, private router: Router) {

    }

    ngOnInit(): void {

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onCheckTerms(args) {
        let secondSwitch = <Switch>args.object;

        if (secondSwitch.checked) {
            this.switchState = secondSwitch.checked;
        }
    }

    onValueTransfer(value) {
        this[value.label.replace(/\s/g, '')
            .toLowerCase()] = value.value;

    }

    usernameAvaliableCheck() {
        const registerCred = {
            username: this.username,
            password: this.password,
            confirmPassword: this.confirmpassword,
            email: this.email,
            accountNumber: this.dealeraccountnumber,
            serialNumber: this.tankmonitorserial,
            phoneNumber: this.phonenumber
        }
        console.log("sending over", registerCred)
        if (this.validations(registerCred)) {
            this.userService.isUsernameAvaliable(registerCred).then((res) => {
                if (res.isAvailable) {
                    this.register(registerCred)
                } else {
                    let options = {
                        title: "Username unavailable",
                        message: "Please choose a new username",
                        okButtonText: "OK"
                    };

                    alert(options)
                }


            }).catch((error) => {
                let options = {
                    title: "Issue Occured",
                    message: "Account might not have been created",
                    okButtonText: "OK"
                };

                alert(options)

            })
        }
    }

    validations(cred) {

        if (!cred.username) {
            this.error = "Please enter your username.";
        }

        if (!cred.email) {
            this.error = "Please enter your email.";

        }

        if (!cred.password) {
            this.error = "Please enter your password.";

        } else if (!cred.confirmPassword) {
            this.error = "Please confirm your password.";
        }

        if (cred.password && cred.confirmPassword && cred.password !== cred.confirmPassword) {
            this.error = "These passwords do not match.";
        }

        if (!cred.phoneNumber) {
            this.error = "Please enter your phone number";
        }

        if (cred.phoneNumber && cred.phoneNumber.length !== 10) {
            this.error = "Please enter a valid phone number";
        }
        if (!cred.accountNumber) {
            this.error = "Please enter your account number.";
        }

        if (!cred.serialNumber) {
            this.error = "Please enter your monitor number.";
        }

        if(this.switchState) {
            this.error = "Please Agree to the Terms and Conditions";
        }

        if (this.error) {
            return false;
        } else {
            return true;
        }

    }

    register(cred) {
        this.userService.register(cred).then((res) => {
            this.router.navigate(["/login"]);

        }).catch((error) => {
            console.log("Register Error", error);
            let options = {
                title: "Registering Failed",
                message: "Please try again",
                okButtonText: "OK"
            };

            alert(options);
        })
    }


    launchWebSite() {
        utilityModule.openUrl("http://support.gremlinmonitors.com/#/Tos");
    }


    public scanTapped(): void {

        this.barcodeScanner.scan({
            formats: "QR_CODE, EAN_13",
            beepOnScan: true,
            reportDuplicates: true,
            preferFrontCamera: false
            // continuousScanCallback: scanResult => {
            //   console.log("result: " + JSON.stringify(scanResult));
            //   this.barcodeScanner.stop();
            // }
        })
            .then((result) => {
                console.log("Barcode Success", JSON.stringify(result));
                this.tankmonitorserial = result.text.trim().replace(/^0+/, '');
            })
            .catch((error) => {
                let options = {
                    title: "Scan Failed",
                    message: "Please try again or enter manually",
                    okButtonText: "OK"
                };

                alert(options)
            });
        // };
        // this.barcodeScanner.hasCameraPermission()
        //     .then(granted => granted ? scan() : console.log("Permission denied"))
        //     .catch(() => {
        //         this.barcodeScanner.requestCameraPermission()
        //             .then(() => scan());
        //     });
    }

}
