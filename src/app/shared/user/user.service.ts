import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { CommsService } from "../comms";
import { Config } from "../config";
import { User } from "./user";

@Injectable()
export class UserService {
    private loggedIn = new BehaviorSubject(false);
    loggedInState = this.loggedIn.asObservable();
    private logoUrl = new BehaviorSubject(" ");
    currentLogoUrl = this.logoUrl.asObservable();
    constructor(private commsService: CommsService) {
    }
    changeLoggedIn(state: boolean){
        console.log("Logged In State", state)
        this.loggedIn.next(state);

    }

    login(user: User) {
        interface UserReturnData {
            token: string;
            user: {
                subscriberId: string,
                dealerId: string
            };
        }

        const apiLoginUrl: string = this.commsService.getEndPoint("auth", user.username);

        const userLogin = {
            username: user.username.trim(),
            password: user.password.trim()
        };

        return this.commsService.request(apiLoginUrl, "post", userLogin)
            .then((data: UserReturnData) => {
                console.log("line 27", typeof data, data)

                Config.token.access = data.token;
                Config.token.subscriberId = data.user.subscriberId;
                Config.token.dealerId = data.user.dealerId;

                this.getLogo();

            });
    }

    getLogo() {
        const apiLogoUrl: string = this.commsService.getEndPoint("logo", Config.token.subscriberId);

        this.commsService.request(apiLogoUrl, "get", {}, "text", "text/plain")
            .then((logo) => {
                console.log("data from logo", logo)
                this.logoUrl.next(logo);

            }).catch((error) => console.log("Getting Logo Error", error));

    }

    reset(username: string) {
        const apiResetUrl: string = this.commsService.getEndPoint("reset", username);

        return this.commsService.request(apiResetUrl, "post", {username})
            .then((res) => {
                console.log("Password Reset", res);
            }).catch((error) => console.log("line 64", error));
    }

    isUsernameAvaliable(cred) {
        const apiUsernameAvaliabletUrl: string = this.commsService.getEndPoint("isUsernameAvailable", cred.username);

        return this.commsService.request(apiUsernameAvaliabletUrl, "get")
            .then((res) => {
                console.log("Username", res);
              return res;
            }).catch((error) => console.log("line 64", error));
    }
    register(cred) {
        const apiRegisterUrl: string = this.commsService.getEndPoint("register", cred.username);
        console.log("registeringapi", apiRegisterUrl )

        return this.commsService.request(apiRegisterUrl, "post", cred)
    }

}
