import {HttpClient, HttpEvent, HttpHeaders} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class CommsService {
    logo: Function;
    isUsernameAvailable: Function;
    deliveries: Function;
    dealer: Function;
    auth: string;
    reset: string;
    register: string;
    consumer: string;
    level: string;
    baseEndPoints: object;
    baseEndPoint: string;

    constructor(private http: HttpClient) {

        this.logo = (subscriber) => `/consumer/api/subscribers/${subscriber}/logo`;
        this.isUsernameAvailable = (username) => `/consumer/usernameAvailable/${username}`;
        this.deliveries = (tank) => `/consumer/api/tanks/${tank}/levels`;
        this.dealer = (subscriber, dealer) => `/consumer/api/subscribers/${subscriber}/tmsContacts?dealerId=${dealer}`;
        this.auth = "/consumer/signIn";
        this.reset = "/consumer/forgotPassword";
        this.register = "/consumer/register";
        this.consumer = "/consumer/api";
        this.level = "/tmsLevels";
        this.baseEndPoints = {
            prod: "https://support.paygo.net",
            qa: "https://qa.angusdev.com",
            dev: "https://dev.angusdev.com",
            feature: "https://paygo.angusdev.com",
            local: "http://localhost:3000",
        };
        this.baseEndPoint = "";
    }

    getEndPoint(route: string, ...info: string[]) {
        console.log("BaseEndpoint", this.baseEndPoint)
        let assertUsername: string;
        let routeString: any;
console.log("this is info", info)
        if(!this.baseEndPoint || (info[0] && typeof info[0] === "string" && info[0].includes("_")) ) {
            assertUsername = info[0].trim().toLowerCase();

            for (let environment in this.baseEndPoints) {
                if (assertUsername.startsWith(`${environment.toLowerCase()}_`)) {
                    this.baseEndPoint = this.baseEndPoints[environment];
                    break;
                }
                this.baseEndPoint = this.baseEndPoints["prod"];
            }
        }

        routeString = this[route];
        if (typeof routeString !== 'string') {
            routeString  = routeString.apply(null, info);
        }

        return this.baseEndPoint + routeString;

    }

    request(url: string, method: string, data: any = {}, responseType: string = "json", contentType: string = "application/json") {
        const headers = new HttpHeaders().set("Content-Type", contentType);
        console.log("Request Being Sent", method, url, data,  responseType, contentType);

        return this.http[method](
                url,
                data,
            {headers}
            ).toPromise()
    }

}