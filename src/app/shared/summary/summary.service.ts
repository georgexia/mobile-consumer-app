import { Injectable } from "@angular/core";
import * as moment from "moment";
import { BehaviorSubject } from "rxjs";
import { Config } from "../config";
import { CommsService } from "../comms";


@Injectable()
export class SummaryService {
    private tankSource = new BehaviorSubject(0);
    currentTank = this.tankSource.asObservable();
    private dealerInfo = new BehaviorSubject({
        email:" ",
        phone:" ",
        url:" ",
        subsriberId:" ",
        id:" "});
    currentdealerInfo = this.dealerInfo.asObservable();

    private consumerInfo = new BehaviorSubject({
        tanks: [],
        customer: {
            account: "",
            name: "",
            address:{
                line1: "",
                line2: "",
                city: "",
                state: "",
                postalCode: ""

            },
            phone: ""
        }
    });
    currentConsumerInfo = this.consumerInfo.asObservable();
    constructor(private commsService: CommsService) {}

    changeTank(tank: number) {
        console.log("this is changing the tank", tank)
        this.tankSource.next(tank);
    }

    changeConsumerInfo(consumerInfo: any) {
        console.log("this is changing the consumerinfo", consumerInfo)
        this.consumerInfo.next(consumerInfo);
    }

// These methods are related to creating the data structure with tank levels and percentage

    loadConsumerData() {
        interface UserResponseData {
            tanks: Array<object>;
            customerName: string;
            accountNum: string;
            address: object;
            phoneNumber: string;

        };

        interface TanksFormat {
            tanks: Array<{key: string, hasLevel: boolean, percent: number, levels: any}>,
            customer: {account: string, name: string, address:{line1: string, line2: string, city: string, state:string, postalCode: string}, phone: string}

        };

        let apiConsumerUrl: string = this.commsService.getEndPoint("consumer");

        return this.commsService.request(apiConsumerUrl, "get" )
            .then((response: UserResponseData) => {
                return this.tanksFormat(response);

            }).then((tanksFormatRes: TanksFormat) => {
                return Promise.all( tanksFormatRes.tanks.map(
                    tank => {
                        return this.loadDeliveries(tank.key)

                    }
                )).then(resList => {


                    tanksFormatRes.tanks.map((tank, index) => {
                        const levels = resList[index][0];

                        if (levels.percent) {
                            tank.hasLevel = true;
                            tank.percent = Math.floor(Math.max(Math.min(levels.percent, 100), 0));


                        } else {
                            tank.hasLevel = false;
                            tank.percent = 0;

                        }

                        tank.levels = resList[index];

                        return tank;
                    });
                    //Config.consumerInfo = tanksFormatRes;

                    this.changeConsumerInfo(tanksFormatRes)
                    //this.consumerInfo.next(tanksFormatRes);
                    return tanksFormatRes;

                });


            })
    }

    tanksFormat(receivedTanks) {
        interface UserResponseTank {
            name: any;
            lastDeliveryDate: string;
            hasLastDate: boolean;
            originalLastDeliveryDate: string;
            nextDeliveryDate:string;
            hasNextDate: boolean;
            originalNextDeliveryDate: string;
        };

        let tanks = receivedTanks.tanks
            .map((tank: UserResponseTank) => {
                if (!isNaN(tank.name)) {
                    tank.name = `Tank #${tank.name}`;
                }

                if (!tank.lastDeliveryDate) {
                    tank.hasLastDate = false;
                } else {
                    tank.hasLastDate = true;
                    tank.originalLastDeliveryDate = tank.lastDeliveryDate;
                    tank.lastDeliveryDate = moment(tank.lastDeliveryDate, "YYYY-MM-DD").format('MM/DD/YYYY');
                }

                if (!tank.nextDeliveryDate) {
                    tank.hasNextDate = false;
                } else {
                    var _date = moment(tank.nextDeliveryDate, "YYYY-MM-DD");
                    if (!_date.add(1, 'days').isAfter(moment())) {
                        //Date is in the past
                        tank.hasNextDate = false;

                    } else {
                        tank.hasNextDate = true;
                        tank.originalNextDeliveryDate = tank.nextDeliveryDate;
                        tank.nextDeliveryDate = moment(tank.nextDeliveryDate, "YYYY-MM-DD").format('MM/DD/YYYY');
                    }

                }

                return tank;
            });
        return {
            customer: {
                name: receivedTanks.customerName,
                account: receivedTanks.accountNum,
                address: receivedTanks.address,
                phone: receivedTanks.phoneNumber,
                hasPhone: !!receivedTanks.phoneNumber
            },
            tanks: tanks
        };

    };

    loadDeliveries(tankKey) {
        interface DeliveriesRes {
            levels: Array<{readingDate: string, originalReadingDate: string, percent: number, length: Array<object>}>;
        }
        let apiDeliveriesUrl: string = this.commsService.getEndPoint("deliveries", tankKey);

        return this.commsService.request(apiDeliveriesUrl, "get")
            .then((response: DeliveriesRes) =>  response.levels
                .sort((a,b) => moment.utc(b.readingDate).diff(moment.utc(a.readingDate)) )
                .slice(0,30)
                .map(level => {
                    level.originalReadingDate = level.readingDate;
                    level.readingDate = moment(level.readingDate, "YYYY-MM-DD").format('MM/DD/YYYY');
                    return level;
                })
            );

    };

    loadDealerData() {
        let apiDealerUrl: string = this.commsService.getEndPoint("dealer",Config.token.subscriberId, Config.token.dealerId);

        return this.commsService.request(apiDealerUrl, "get").then((data) => {
            this.dealerInfo.next(data);

        });

    }




}
