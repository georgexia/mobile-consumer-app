import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { Observable } from "rxjs";
import { Config } from "./config";

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        console.log("intercepted request ... ", req);

        // Clone the request to add the new header.
        const authReq = req.clone(
            {
                headers: req.headers.set("x-access-token", Config.token.access),
                responseType: 'text'
            }
        );

        console.log("Sending request with new header now ...", authReq);

        //send the newly created request
        return next.handle(authReq);
    }
}