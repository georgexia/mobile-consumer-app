import { Injectable } from "@angular/core";
import { CommsService } from "../comms";
import { BehaviorSubject } from "rxjs";
import {Config} from "~/app/shared/config";


@Injectable()
export class MonitorResetService {
    private ssids = new BehaviorSubject([]);
    currentSsids = this.ssids.asObservable();
    constructor(private commsService: CommsService) {

    }

    getMonitorId() {
        console.log("monitor service reset")

        return this.commsService.request("http://192.168.4.1", "get", {}, "text").then((res) => {
            console.log("Response from Monitor Ssid",typeof res,  res)
            let ssids = [];
            // <option value=\"(.*?)\">
            // <option\b[^>]*>(.*?)<\/option>
            // <td class="style23" align="left">(.|\n)(.*?)<\/td>
            const matchSsid = /<option value=\"(.*?)\">/g;
            // const matchSerial = /<td class="style23" align="left">((.|\n)*?)<\/td>/g;
            let matched;
            do {
                matched = matchSsid.exec(res);
                if (matched && !(matched[1] === "choose_from_list") && !(matched[1] === "manually_entered")) {
                    console.log("SSID recieved", matched[1]);
                    ssids.push(matched[1]);
                }
            } while (matched);
            this.ssids.next(ssids);

            return ssids;
        });

    }
    sendMonitorResponse(data) {
        console.log("this is the sendMonitorResponse", data)

        return this.commsService.request("http://192.168.4.1/config?command=wifi", "post", JSON.stringify(data), "", 'text/plain' );

    }
}
